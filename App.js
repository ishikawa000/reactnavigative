import React, {
    Component,
} from 'react'

import {
    AppRegistry,
    View,
    Text,
    Button,
} from 'react-native'

const MainScreenView = {
    LEFT_SCREEN: 'LEFT_SCREEN',
    CENTER_SCREEN: 'CENTER_SCREEN',
    RIGHT_SCREEN: 'RIGHT_SCREEN',
}

export default class App extends Component {
    constructor(props) {
        super(props)
        this.state = {currentView: MainScreenView.LEFT_SCREEN}
    }

    renderMainScreen() {
        return (
            <View>
                <Text>{this.state.currentView}</Text>
            </View>
        )
    }

    changeMainScreenView(mainScreenView) {
        this.setState({currentView: mainScreenView})
    }

    render() {
        return (
            <View style={{flex: 1, flexDirection: 'column'}}>
                {this.renderMainScreen()}
                <View style={{flex: 1, flexDirection: 'row', height: 10}}>
                    <Button title="left" onPress={() => this.changeMainScreenView(MainScreenView.LEFT_SCREEN)}/>
                    <Button title="center" onPress={() => this.changeMainScreenView(MainScreenView.CENTER_SCREEN)}/>
                    <Button title="right" onPress={() => this.changeMainScreenView(MainScreenView.RIGHT_SCREEN)}/>
                </View>
            </View>
        )
    }
}

AppRegistry.registerComponent('ReactNavigative', () => App)
